#!/usr/bin/env python3

### static configuration

SERVER_CONFIG = "/etc/wireguard/wg0.conf"
CLIENT_CONFIG_TEMPLATE = "/etc/wireguard/client_configs/_template"

import argparse, ipaddress, os

### Argument parser

parser = argparse.ArgumentParser()
parser.add_argument("-F", "--full-name", type = str,
                    help = "User's full name.")
args = parser.parse_args()

### Helper functions
def normalize(name) -> str:
    name = name.lower()

    rd = {  'á': 'a', 'ä': 'a',
            'ć': 'c', 'č': 'c',
            'ď': 'd',
            'é': 'e', 'ě': 'e', 'ë': 'e',
            'í': 'i', 'ï': 'i',
            'ľ': 'l',
            'ň': 'n',
            'ó': 'o', 'ö': 'o',
            'ř': 'r',
            'š': 's',
            'ť': 't',
            'ú': 'u', 'ů': 'u', 'ü': 'u',
            'ý': 'y',
            'ž': 'z'
         }
    for key in rd.keys():
        name = name.replace(key, rd[key])
    return (name)

### Main code
privkey = os.popen("wg genkey").read().rstrip()
pubkey = os.popen("echo %s | wg pubkey" % privkey).read().rstrip()

# find the last occurence of "AllowedIPs" and increase the IP by 1
ip = ""
for line in reversed(open(SERVER_CONFIG, "r").readlines()):
  if ("AllowedIPs" in line):
    ip, mask = line.split()[-1].split("/") # "172.17.25.1", "24"
    ip = ipaddress.ip_address(ip) # convert to IP address
    ip += 1
    ip = str(ip) + "/" + mask # "172.17.25.2/24"
    break

## Client config generation
client_config = ""
with open(CLIENT_CONFIG_TEMPLATE, "r") as template:
  client_config = template.read() % (privkey, ip)

print("Client configuration:\n")
print(client_config + "\n")

## Server config generation
server_config = """# %s
[Peer]
AllowedIPs = %s
PublicKey = %s

""" % (args.full_name, ip, pubkey)

print("Server configuration:\n")
print(server_config + "\n")

## Confirm
input_confirm = input("Are you sure?\n> ")

if (not (input_confirm == 'y')):
  exit(1)

# "Žluťoučký Kůň" -> "kunzlutoucky"
username = "".join(list(reversed(normalize(args.full_name).split())))

with open("/etc/wireguard/client_configs/%s.conf" % username, "w") as f_client_config:
  f_client_config.write(client_config)

with open(SERVER_CONFIG, "a") as f_server_config:
  f_server_config.write(server_config)

privkey = os.popen("wg syncconf wg0 <(wg-quick strip wg0)")