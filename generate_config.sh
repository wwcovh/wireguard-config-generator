#!/bin/bash
#ocekava uzivatelske jmeno jako parametr 1, pokud je parametr 1 prazdny,
#vyzve k zadani jmena

#Promenne pro nastaveni dle aktualniho serveru
Server_PublicKey="TBA"
AllowedIPs="TBA, TBA"
Endpoint="TBA"
PersistentKeepalive="20"
Server_Config_File="/etc/wireguard/wg0.conf"

#--------------------------------------------------------------------------
#vlastni skript
#zadani uzivatelskeho jmena a vytvoreni prazdneho konfiguracniho souboru
if [ -z $1 ]; then
    read -p "Zadej uzivatelske jmeno: " name
    filename="client_"$name".conf"
else
    filename="client_"$1".conf"
fi

touch $filename

#zjisteni posledni pouzite IP adresy pro klienta
#nehleda nejvyssi moznou IP, hleda posledni radek s AllowedIPs
#v konfig. souboru serveru
LastClientAllowedIP=$(grep AllowedIPs $Server_Config_File | tail -1)
Last_IP=$(echo $LastClientAllowedIP | cut -d "." -f 4 | cut -d "/" -f 1)
Last_IP=$(($Last_IP+1))
Subnet=$(echo $LastClientAllowedIP | cut -d "/" -f 2)
New_IP=$(echo $LastClientAllowedIP | cut -d "." -f 1)"."$(echo $LastClientAllowedIP | cut -d "." -f 2)"."$(echo $LastClientAllowedIP | cut -d "." -f 3)"."$Last_IP"/"$Subnet

#vygenerovani verejneho a soukromeho klice
#umask 077; wg genkey | tee private.key | wg pubkey > public.key
#PubKey=$(cat public.key)
#PrivKey=$(cat private.key)
#rm public.key
#rm private.key
PrivKey=$(wg genkey)
PubKey=$(wg pubkey<<<$PrivKey)

#pridani konfigurace klienta do konfig. souboru serveru
echo -e '\n\n#'$name'\n[PEER]\nPublicKey='$PubKey'\n'$New_IP >> $Server_Config_File

#vytvoreni vlastniho konfiguracniho souboru klienta
New_IP=$(echo $New_IP | cut -d " " -f 3)
echo -e '[Interface]\nPrivateKey = '$PrivKey'\nAddress = '$New_IP'\n\n[Peer]\nPublicKey = '$Server_PublicKey'\nAllowedIPs = '$AllowedIPs'\nEndpoint = '$Endpoint'\nPersistentKeepalive = '$PersistentKeepalive >> $filename

systemctl restart wg-quick@*